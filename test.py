import pukala

def zero():
    dt = {
        "nothing": "yes"
    }
    assert(pukala.do_exec(dt) == dt)

def basic_fcall():
    dt = {
        "buba": "1",
        "result": ["_fcall", "get_val", "/buba"]
    }
    assert(pukala.do_exec(dt) == {
        "buba": "1",
        "result": "1"
    })

def nested_fcall():
    dt = {
        "buba": "1",
        "truba": "2",
        "result": ["_fcall", "do_fmt", "%s-%s", ["_fcall", "get_val", "/buba"], ["_fcall", "get_val", "/truba"]]
    }
    assert(pukala.do_exec(dt) == {
        "buba": "1",
        "truba": "2",
        "result": "1-2"
    })

def nested_nested_fcall():
    dt = {
        "buba": "1",
        "truba": "2",
        "kruba": "3",
        "result": ["_fcall", "do_fmt", "%s-%s", ["_fcall", "get_val", "/buba"], ["_fcall", "do_fmt", "%s-%s", ["_fcall", "get_val", "/truba"], ["_fcall", "get_val", "/kruba"]]]
    }
    assert(pukala.do_exec(dt) == {
        "buba": "1",
        "truba": "2",
        "kruba": "3",
        "result": "1-2-3"
    })


def cobj_fcall():
    class MyCobj(pukala.BaseCobj):
        def do_something(self):
            return "something"
    dt = {
        "pushing": ["_fcall", "push_cobj", "my_cobj"],
        "result": ["_fcall", "do_something"]
    }
    assert(pukala.do_exec(dt, {
        "my_cobj": MyCobj
    }) == {
        "pushing": None,
        "result": "something"
    })

string_path = basic_fcall

def arr_path():
    dt = {
        "buba": "1",
        "result": ["_fcall", "get_val", ["buba"]]
    }
    assert(pukala.do_exec(dt) == {
        "buba": "1",
        "result": "1"
    })

def relative_arr_path():
    dt = {
        "buba": "1",
        "result": ["_fcall", "get_val", ["..", "buba"]],
        "truba": {
            "result": ["_fcall", "get_val", ["..", "..", "buba"]]
        },
        "truba2": [
            ["_fcall", "get_val", ["..", "..", "buba"]]
        ],
    }
    assert(pukala.do_exec(dt) == {
        "buba": "1",
        "result": "1",
        "truba": {
            "result": "1"
        },
        "truba2": [
            "1"
        ],
    })

def relative_string_path():
    dt = {
        "buba": "1",
        "result": ["_fcall", "get_val", "../buba"],
        "truba": {
            "result": ["_fcall", "get_val", "../../buba"]
        },
        "truba2": [
            ["_fcall", "get_val", "../../buba"]
        ]
    }
    assert(pukala.do_exec(dt) == {
        "buba": "1",
        "result": "1",
        "truba": {
            "result": "1"
        },
        "truba2": [
            "1"
        ],
    })

def fcall_creates_fcall():
    class MyCobj(pukala.BaseCobj):
        def do_something0(self):
            return ["_fcall", "do_something"]
        def do_something(self):
            return "something"
    dt = {
        "pushing": ["_fcall", "push_cobj", "my_cobj"],
        "result": ["_fcall", "do_something0"],
        "result2": [
            ["_fcall", "do_something0"]
        ],
    }
    assert(pukala.do_exec(dt, {
        "my_cobj": MyCobj
    }) == {
        "pushing": None,
        "result": "something",
        "result2": ["something"],
    })

def unlater():
    dt = {
        "buba": 1,
        "fn0": ["_fcall_later", "get_val", ["buba"]],
        "fn1": ["_fcall", "unlater", "/fn0"]
    }
    assert(pukala.do_exec(dt) == {
        'buba': 1,
        'fn0': ['_fcall_later', 'get_val', ['buba']],
        'fn1': 1
    })

def do_delete():
    dt = {
        "buba": 1,
        "fn0": ["_fcall", "delete_res", ["_fcall", "get_val", ["buba"]]],
        "fn1": [
            ["_fcall", "delete_res", ["_fcall", "get_val", ["buba"]]],
            123
        ],
    }
    assert(pukala.do_exec(dt) == {
        'buba': 1,
        "fn1": [
            123
        ]
    })

def do_merge():
    dt = {
        "buba": {
            "pupse": 7
        },
        "buba2": [
            "pupse"
        ],
        "fn0": ["_fcall", "merge_res", ["_fcall", "get_val", ["buba"]]],
        "fn1": [
            ["_fcall", "merge_res", ["_fcall", "get_val", ["buba2"]]],
            123
        ]
    }
    assert(pukala.do_exec(dt) == {
        'buba': {
            "pupse": 7
        },
        "buba2": [
            "pupse"
        ],
        "pupse": 7,
        "fn1": [
            "pupse",
            123
        ]
    })

def array_index_in_path():
    dt = {
        "buba": [
            [
                123
            ]
        ],
        "fn0": ["_fcall", "get_val", ["buba", 0, 0]],
    }
    assert(pukala.do_exec(dt) == {
        "buba": [[123]],
        "fn0": 123
    })

def path_push_path():
    dt = {
        "buba": [
            [
                123
            ]
        ],
        "__push": ["_fcall", "delete_res", ["_fcall", "push_path", "/buba/0"]],
        "fn0": ["_fcall", "get_val", [".", 0]],
    }
    assert(pukala.do_exec(dt) == {
        'buba': [[123]],
        'fn0': 123
    })    

    
cases = [
    zero,
    basic_fcall,
    nested_fcall,
    nested_nested_fcall,
    cobj_fcall,
    string_path,
    arr_path,
    relative_string_path,
    relative_arr_path,
    fcall_creates_fcall,
    unlater,
    do_delete,
    do_merge,
    array_index_in_path,
    path_push_path,
]

def main():
    for i in cases:
        i()

if __name__ == "__main__":
    main()

