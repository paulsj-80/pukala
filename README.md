# The vision
Imagine growing a tree by creating new and new branches to it
without cutting single one...

# In other words
It is common habit to replace global variables with local ones, thus
making program safer and more understandable. Still, if global
variable is set just once and is immutable afterwards, it is not
that important to do that; in contrary, such data is kept global
as constants. Most source code - classes, functions, etc. - of any
system can be regarded as an "immutable state". Development of ideas
can happen more freely if all things are in front of one's eyes, in
a structured way. All that put together resulted in this project.

# Usability
Main intent is to have system integration tool, which would allow
to configure various languages, machines and services from
single place; dict/list format is generic one and can be easily
translated to YAML, JSON, XML, shell scripts, pretty much
anything; ofc, it can be used for other purposes as well. As the
tree is only grown, and evaluated branches are immutable, it is
easier to diagnose problems; end result can be stored, compared,
used as a base for another tree, etc. "Canonical" use case would be
a situation when one has a tested and working configuration which
needs to be deployed on many machines after small modifications. Or
have a master configuration file for complex test cases which would
involve many configurable components.

# Terms and concepts
- **doctree**: composition of dictionaries, lists and simple data
  types; first level is always dictionary; one can visualize it as
  JSON or YAML file
- **fcall**: array with specific format which is treated as function
  call description; contains following elements:
  - _fcall keyword
  - function name: refers function in in context object
  - function parameters; any parameter (and function name, too) can be
    fcall itself, forming nested call
- **context object**: object which contains all necessary functions
  to evaluate doctree; two properties: purity and dependence on
  doctree; for impure or dependent some precautions should be
  taken to ensure they are evaluated in correct order, by
  constructing the tree and fcall priorities
- **nested fcall**: is a fcall inside other fcall parameter list
- **fcall evaluation**: calling function, defined in fcall, and storing
  result in doctree by substituting fcall; evaluation may return
  another fcall
- **doctree evaluation**: evaluating all fcalls in natural (top-down)
  order; if fcall returns another fcall, it is evaluated next
- **immutability**: branches are only added; not allowed (admissed) to
  alter a tree from inside of fcall otherwise than by providing function
  return value; this is to promote clean design