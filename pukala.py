import copy
import sys
import importlib

VERSION="3.0.3"

class PukalaException(Exception):
    pass

class Path:

    @staticmethod
    def to_arr(path):
        def trim_empties(arr):
            return [i for i in arr if i != "" and i is not None]
        if path is None:
            return []
        if type(path) == str:
            if path and path[0] not in (".", "/"):
                path = "./%s" % path
            res = path.split("/")
            return trim_empties(res)
        if type(path) == list:
            return trim_empties(path)
        raise PukalaException("bad path %s" % path)
    
    @staticmethod
    def is_absolute(path):
        path = Path.to_arr(path)
        return path == [] or path[0] not in (".", "..")
    
    @staticmethod
    def normalize(path, path_from=None):
        path = Path.to_arr(path)
        if Path.is_absolute(path):
            return path
        path_from = Path.to_arr(path_from)
        if not Path.is_absolute(path_from):
            raise PukalaException("expected absolute path, got %s" % path_from)
        res = []
        for i in path_from + path:
            if i == ".":
                pass
            elif i == "..":
                res.pop()
            else:
                res.append(i)
        return res

    @staticmethod
    def get_path_stack(tree, path, path_from=None):
        path = Path.normalize(path, path_from)
        ts = [tree]
        for i in path:
            if i == ".":
                pass
            elif i == "..":
                ts.pop()
            else:
                if type(ts[-1]) == list:
                    i = int(i)
                ts.append(ts[-1][i])
        return ts

    @staticmethod
    def get_subtree(tree, path, path_from=None):
        return Path.get_path_stack(tree, path, path_from)[-1]

class ExecContext:
    def __init__(self, doctree, cobj_reg):
        self.doctree = doctree
        base_cobj = BaseCobj()
        self.cobj_reg = cobj_reg
        self.cobj_stack = [base_cobj]
        self.path_stack = []

class CallContext:
    def __init__(self, path, args, exec_context):
        self.path = path
        self.args = args if args else []
        self.exec_context = exec_context

class FCall:
    @staticmethod
    def is_fcall(item):
        return type(item) == list and len(item) > 0 and item[0] == "_fcall"
    
    @staticmethod
    def is_fcall_later(item):
        return type(item) == list and len(item) > 0 and item[0] == "_fcall_later"

    @staticmethod
    def unlater(item):
        if not FCall.is_fcall_later:
            raise Exception("not a fcall_later %s" % item)
        return ["_fcall"] + item[1:]

    @staticmethod
    def get_resp_props(res):
        if type(res) == tuple:
            return res[0]
        return {}

    @staticmethod
    def do_exec(tree, path, exec_context, fcall):
        if not FCall.is_fcall(fcall):
            raise PukalaException("not an fcall %s" % str(fcall))
        args = []
        for i in fcall[1:]:
            if FCall.is_fcall(i):
                args.append(FCall.do_exec(tree, path, exec_context, i))
            else:
                args.append(i)
        if "." in args[0]:
            ct = args[0].split(".")
            cobj = exec_context.cobj_reg[ct[0]]
            fn = getattr(cobj, ct[1])
        else:
            cobj = exec_context.cobj_stack[-1]
            fn = getattr(cobj, args[0])
        context = CallContext(path, args[1:], exec_context)
        return fn(context)
    

class BaseCobj:
    def __init__(self):
        self.storage = {}
    def push_path(self, context):
        context.exec_context.path_stack.append(context.args[0])
    def pop_path(self, context):
        context.exec_context.path_stack.pop(-1)
    def push_cobj(self, context):
        cobj_name = context.args[0]
        if not cobj_name in context.exec_context.cobj_reg:
            raise PukalaException("unknown cobj_name %s" % cobj_name)
        cobj = context.exec_context.cobj_reg[cobj_name]
        context.exec_context.cobj_stack.append(cobj)
    def pop_cobj(self, context):
        context.exec_context.cobj_stack.pop(-1)
    def get_storage(self):
        return self.storage
    def get_val(self, context):
        path = context.args[0]
        ps = context.exec_context.path_stack
        path_from = ps[-1] if len(ps) > 0 else context.path
        return Path.get_path_stack(context.exec_context.doctree, path, path_from)[-1]
    def do_fmt(self, context):
        return context.args[0] % tuple(context.args[1:])

    def delete_res(self, context):
        return ("delete",)

    def merge_res(self, context):
        return ("merge",) + tuple(context.args)

    def unlater(self, context):
        path = context.args[0]
        ps = context.exec_context.path_stack
        path_from = ps[-1] if len(ps) > 0 else context.path
        src = Path.get_path_stack(context.exec_context.doctree, path, path_from)[-1]        
        return unlater(src)

    def load_module(self, context):
        path = context.args[0]
        module_name = context.args[1]
        class_name = context.args[2]
        cobj_name = context.args[3]
        if path:
            sys.path.append(path)
        m = importlib.import_module(module_name)
        context.exec_context.cobj_reg[cobj_name] = getattr(m, class_name)()
        return context.exec_context.cobj_reg[cobj_name].init(context)
    

def do_exec(doctree, cobj_reg=None, max_container_size=1000, max_nested_fcalls=1000):
    if FCall.is_fcall(doctree):
        raise PukalaException("cannot evaluate bare fcall; need dict/list context")
    doctree = copy.deepcopy(doctree)
    if not cobj_reg:
        cobj_reg = {}

    def get_res(cp, fcall):
        res = FCall.do_exec(doctree, cp, exec_context, fcall)
        if FCall.is_fcall(res):
            return get_res(cp, res)
        return res
    
    def traverse_container(root, cp, get_key_val_cb, yield_items_cb, add_to_container_cb):
        do_repeat = True
        last_succ = -1
        while do_repeat:
            if len(root) > max_container_size:
                raise Exception("max_container_size exceeded while evaluating %s" % cp)

            do_repeat = False
            for idx,i in enumerate(yield_items_cb(root, last_succ+1)):
                key, value = get_key_val_cb(idx, i)
                if FCall.is_fcall_later(value):
                    root[key] = value
                    last_succ = idx
                    continue
                if not FCall.is_fcall(value):
                    root[key] = next_call(value, cp + [key])
                    last_succ = idx
                    continue
                res = get_res(cp + [key], value)
                if not type(res) == tuple:
                    root[key] = res
                    root[key] = next_call(res, cp + [key])
                    last_succ = idx
                    continue
                if res[0] == "merge":
                    do_repeat = True
                    root2 = []
                    for idx2,j in enumerate(yield_items_cb(root, 0)):
                        if idx2 == idx:
                            for k in yield_items_cb(res[1], 0):
                                root2.append(copy.deepcopy(k))
                            continue
                        root2.append(j)
                    root.clear()
                    # need to rebuild completely, as any fcall
                    # requires doctree to have all previous fcall
                    # results
                    for j in root2:
                        add_to_container_cb(root, j)
                    break
                elif res[0] == "delete":
                    do_repeat = True
                    root2 = []
                    for idx2,j in enumerate(yield_items_cb(root, 0)):
                        if idx2 == idx:
                            continue
                        root2.append(j)
                    root.clear()
                    for j in root2:
                        add_to_container_cb(root, j)
                    break
                else:
                    raise PukalaException("bad fcall result op: %s" % res)
    
    exec_context = ExecContext(doctree, cobj_reg)
    def next_call(root, cp):
        if type(root) == dict:
            traverse_container(root, cp, lambda idx, i: i, lambda x,o: list(x.items())[o:], lambda x, i: x.update(dict([i])))
        elif type(root) == list:
            traverse_container(root, cp, lambda idx, i: (idx, i), lambda x, o: x[o:], lambda x, i: x.append(i))
        return root

    next_call(doctree, [])
    return doctree


def unlater(tree):
    def next_call(root):
        if type(root) == dict:
            res = {}
            for k,v in root.items():
                if Fcall.is_fcall_later(v):
                    v = FCall.unlater(v)
                res[k] = v
            return res
        elif type(root) == list:
            if FCall.is_fcall_later(root):
                return FCall.unlater(root)
            elif Fcall.is_fcall(root):
                return root
            res = []
            for i in root:
                if Fcall.is_fcall_later(i):
                    i = FCall.unlater(i)
                res.append(i)
            return res
        else:
            return root
    return next_call(tree)
